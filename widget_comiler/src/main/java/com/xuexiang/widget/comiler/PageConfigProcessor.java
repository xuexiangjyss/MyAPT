package com.xuexiang.widget.comiler;

import com.alibaba.fastjson.JSON;
import com.google.auto.service.AutoService;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeSpec;
import com.xuexiang.widget.annotation.Page;
import com.xuexiang.widget.enums.CoreAnim;
import com.xuexiang.widget.model.PageInfo;
import com.xuexiang.widget.util.Consts;
import com.xuexiang.widget.util.Logger;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

/**
 * 页面配置自动生成器
 * @author xuexiang
 * @date 2018/4/3 上午12:27
 */
@AutoService(Processor.class)
public class PageConfigProcessor extends AbstractProcessor {
    private Filer mFiler; //文件相关的辅助类
    private Types mTypes;
    private Elements mElements;
    private Logger mLogger; //日志相关的辅助类

    /**
     * 页面配置所在的包名
     */
    private final String mPackageName = "com.xuexiang.xpage";

    private final String mClassName = "PageConfig";

    private TypeMirror mFragment = null;
    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        mFiler = processingEnv.getFiler();              // Generate class.
        mTypes = processingEnv.getTypeUtils();            // Get type utils.
        mElements = processingEnv.getElementUtils();      // Get class meta.
        mLogger = new Logger(processingEnv.getMessager());

        mFragment = mElements.getTypeElement(Consts.FRAGMENT).asType();

        mLogger.info(">>> PageConfigProcessor init. <<<");
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnvironment) {
        if (CollectionUtils.isNotEmpty(annotations)) {
            Set<? extends Element> pageElements = roundEnvironment.getElementsAnnotatedWith(Page.class);
            try {
                mLogger.info(">>> Found Pages, start... <<<");
                parsePages(pageElements);

            } catch (Exception e) {
                mLogger.error(e);
            }
            return true;
        }

        return false;
    }

    /**
     * 解析页面标注
     * @param pageElements
     */
    private void parsePages(Set<? extends Element> pageElements) throws IOException {
        if (CollectionUtils.isNotEmpty(pageElements)) {
            mLogger.info(">>> Found Pages, size is " + pageElements.size() + " <<<");

            ClassName pageConfigClassName = ClassName.get(mPackageName, mClassName);
            TypeSpec.Builder pageConfigBuilder = TypeSpec.classBuilder(pageConfigClassName);

             /*
               private static PageConfig sInstance;
             */
            FieldSpec instanceField = FieldSpec.builder(pageConfigClassName, "sInstance")
                    .addModifiers(Modifier.PRIVATE)
                    .addModifiers(Modifier.STATIC)
                    .build();

            /*

              ``List<PageInfo>```
             */
            ParameterizedTypeName inputListTypeOfPage = ParameterizedTypeName.get(
                    ClassName.get(List.class),
                    ClassName.get(PageInfo.class)
            );

             /*
               private List<PageInfo> mPages = new ArrayList<>();
             */
            FieldSpec pagesField = FieldSpec.builder(inputListTypeOfPage, "mPages")
                    .addModifiers(Modifier.PRIVATE)
                    .build();

            //构造函数
            MethodSpec.Builder constructorBuilder = MethodSpec.constructorBuilder()
                    .addModifiers(Modifier.PRIVATE)
                    .addStatement("mPages = new $T<>()", ClassName.get(ArrayList.class));

            TypeMirror tm;
            String name;
            for (Element element : pageElements) {
                tm = element.asType();

                if (mTypes.isSubtype(tm, mFragment)) {                 // Fragment
                    mLogger.info(">>> Found Fragment Page: " + tm.toString() + " <<<");

                    Page page = element.getAnnotation(Page.class);
                    name = StringUtils.isEmpty(page.name()) ? tm.getClass().getSimpleName() : page.name();

                    constructorBuilder.addStatement("mPages.add(new $T($S, $S, $S, $T.$L))",
                            PageInfo.class,
                            name,
                            tm.toString(),
                            JSON.toJSONString(page.params()),
                            ClassName.get(CoreAnim.class),
                            page.anim());
                }
            }

            MethodSpec constructorMethod = constructorBuilder.build();

            MethodSpec instanceMethod = MethodSpec.methodBuilder("getInstance")
                    .addModifiers(Modifier.PUBLIC)
                    .addModifiers(Modifier.STATIC)
                    .returns(pageConfigClassName)
                    .addCode("if (sInstance == null) {\n" +
                            "    synchronized (PageConfig.class) {\n" +
                            "        if (sInstance == null) {\n" +
                            "            sInstance = new PageConfig();\n" +
                            "        }\n" +
                            "    }\n" +
                            "}\n")
                    .addStatement("return sInstance")
                    .build();

            MethodSpec getPagesMethod = MethodSpec.methodBuilder("getPages")
                    .addModifiers(Modifier.PUBLIC)
                    .returns(inputListTypeOfPage)
                    .addStatement("return mPages")
                    .build();

            CodeBlock javaDoc = CodeBlock.builder()
                    .add("<p>这是PageConfigProcessor自动生成的类，用以自动进行页面的注册。</p>\n")
                    .add("<p><a href=\"mailto:xuexiangjys@163.com\">Contact me.</a></p>\n")
                    .add("\n")
                    .add("@author xuexiang \n")
                    .add("@date ").add(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())).add("\n")
                    .build();

            pageConfigBuilder
                    .addJavadoc(javaDoc)
                    .addModifiers(Modifier.PUBLIC)
                    .addField(instanceField)
                    .addField(pagesField)
                    .addMethod(constructorMethod)
                    .addMethod(instanceMethod)
                    .addMethod(getPagesMethod);
            JavaFile.builder(mPackageName, pageConfigBuilder.build()).build().writeTo(mFiler);
        }
    }


    /**
     * @return 指定哪些注解应该被注解处理器注册
     */
    @Override
    public Set<String> getSupportedAnnotationTypes() {
        Set<String> types = new LinkedHashSet<>();
        types.add(Page.class.getCanonicalName());
        return types;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

}
