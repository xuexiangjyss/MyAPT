package com.xuexiang.widget.util;

/**
 * 常量
 * @author xuexiang
 * @date 2018/4/2 上午12:04
 */
public class Consts {
    // Generate
    public static final String SEPARATOR = "$$";
    public static final String PROJECT = "Widget";
    public static final String TAG = PROJECT + "::";

    public static final String FRAGMENT = "android.support.v4.app.Fragment";

    // Log
    static final String PREFIX_OF_LOGGER = PROJECT + "::Compiler ";

}