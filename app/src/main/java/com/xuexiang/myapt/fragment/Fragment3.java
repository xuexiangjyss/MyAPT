package com.xuexiang.myapt.fragment;

import android.support.v4.app.Fragment;

import com.xuexiang.widget.annotation.Page;
import com.xuexiang.widget.enums.CoreAnim;

/**
 * @author xuexiang
 * @date 2018/4/3 上午2:46
 */
@Page(name = "Fragment3", params = {"param1", "param2"}, anim = CoreAnim.fade)
public class Fragment3 extends Fragment {
}
